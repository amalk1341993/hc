import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,Content,Slides } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-consulthistorymain',
  templateUrl: 'consulthistorymain.html',
})
export class ConsulthistorymainPage {


  currentIndex:any;
  slidertab:any;

  @ViewChild(Content) content: Content;
  @ViewChild('mySlider') slider: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
    let id = this.navParams.get("id");
  this.slidertab = id;
  console.log("id", id);
  setTimeout(() => {
  this.goToSlide(id);
  }, 500)
  this.slidertab = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsulthistorymainPage');
  }
  goToSlide(id) {
this.slider.slideTo(id, 500);
}

slideChanged() {
let currentIndex = this.slider.getActiveIndex();
this.slidertab = currentIndex;
console.log("Current index is", currentIndex);
}

  goBack(){
    this.navCtrl.pop();
  }

  open_page(page){
  this.navCtrl.push(page);
  }

  mainmenu(){
    let modal = this.modalCtrl.create('MenuPage');
    modal.present();
  }

}
