import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsulthistorymainPage } from './consulthistorymain';

@NgModule({
  declarations: [
    ConsulthistorymainPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsulthistorymainPage),
  ],
})
export class ConsulthistorymainPageModule {}
