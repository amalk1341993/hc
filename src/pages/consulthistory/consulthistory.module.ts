import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsulthistoryPage } from './consulthistory';

@NgModule({
  declarations: [
    ConsulthistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsulthistoryPage),
  ],
})
export class ConsulthistoryPageModule {}
