import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-paymenthistory',
  templateUrl: 'paymenthistory.html',
})
export class PaymenthistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymenthistoryPage');
  }

  goBack(){
    this.navCtrl.pop();
  }

  open_page(page){
  this.navCtrl.push(page);
  }

  mainmenu(){
    let modal = this.modalCtrl.create('MenuPage');
    modal.present();
  }
}
