import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController} from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-medicine',
  templateUrl: 'medicine.html',
})
export class MedicinePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicinePage');
  }


  goBack(){
    this.navCtrl.pop();
  }

  open_page(page){
  this.navCtrl.push(page);
  }

  mainmenu(){
    let modal = this.modalCtrl.create('MenuPage');
    modal.present();
  }

}
